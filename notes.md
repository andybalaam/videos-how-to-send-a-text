# How to send a text message notes

## Tab 1 - slides

```
cd ~/Desktop/how-to-send-a-text/
vim slide-*.md
```

## Tab 2 - SMSC

```
cd ~/code/public/cloudhopper-smpp/
git diff
make server
```

## Tab 3 - netcat

```
cd
mkfifo tmpfifo
nc 0.0.0.0 2776 < tmpfifo | xxd
```

## Tab 4 - interactive

```
cd
exec 3> tmpfifo
```

### ENQUIRE_LINK

```
LEN16='\x00\x00\x00\x10'
ENQUIRE_LINK='\x00\x00\x00\x15'
NULL='\x00\x00\x00\x00'
SEQ1='\x00\x00\x00\x01'

echo -n -e "${LEN16}${ENQUIRE_LINK}${NULL}${SEQ1}" >&3
```

Response:

* "00000010" - length=16
* "80000015" - ENQUIRE_LINK_RESP
* "00000000" - success
* "00000001" - matched sequence number

### BIND_TRANSCEIVER

Binding is like logging in.

```
LEN32='\x00\x00\x00\x20'
BIND_TRANSCEIVER='\x00\x00\x00\x09'
NULL='\x00\x00\x00\x00'
SEQ2='\x00\x00\x00\x02'
SYSTEM_ID="sys\x00"
PASSWORD="pas\x00"
SYSTEM_TYPE='typ\x00'
INTERFACE_VERSION='\x34'
ADDR_TON='\x00'
ADDR_NPI='\x00'
ADDRESS_RANGE='\x00'

echo -n -e "${LEN32}${BIND_TRANSCEIVER}${NULL}${SEQ2}${SYSTEM_ID}${PASSWORD}${SYSTEM_TYPE}${INTERFACE_VERSION}${ADDR_TON}${ADDR_NPI}${ADDRESS_RANGE}" >&3
```

Response:

* "00000021" - length=33
* "80000009" - BIND_TRANSCEIVER_RESP
* "00000000" - success
* "00000002" - matched sequence number
* "cloudhopper\0"
* "0134" - TLV??? for interface version?

### SUBMIT_SM

```
LEN61='\x00\x00\x00\x3d'
SUBMIT_SM='\x00\x00\x00\x04'
SEQ3='\x00\x00\x00\x03'
SERVICE_TYPE='\x00'
SOURCE_ADDR_TON='\x00'
SOURCE_ADDR_NPI='\x00'
SOURCE_ADDR='447000123123\x00'
DEST_ADDR_TON='\x00'
DEST_ADDR_NPI='\x00'
DESTINATION_ADDR='447111222222\x00'
ESM_CLASS='\x00'
PROTOCOL_ID='\x01'
PRIORITY_FLAG='\x01'
SCHEDULE_DELIVERY_TIME='\x00'
VALIDITY_PERIOD='\x00'
REGISTERED_DELIVERY='\x01'
REPLACE_IF_PRESENT_FLAG='\x00'
DATA_CODING='\x03'
SM_DEFAULT_MSG_ID='\x00'
SM_LENGTH='\x04'
SHORT_MESSAGE='hihi'
echo -n -e "${LEN61}${SUBMIT_SM}${NULL}${SEQ3}${SERVICE_TYPE}${SOURCE_ADDR_TON}${SOURCE_ADDR_NPI}${SOURCE_ADDR}${DEST_ADDR_TON}${DEST_ADDR_NPI}${DESTINATION_ADDR}${ESM_CLASS}${PROTOCOL_ID}${PRIORITY_FLAG}${SCHEDULE_DELIVERY_TIME}${VALIDITY_PERIOD}${REGISTERED_DELIVERY}${REPLACE_IF_PRESENT_FLAG}${DATA_CODING}${SM_DEFAULT_MSG_ID}${SM_LENGTH}${SHORT_MESSAGE}" >&3
```

* ESM_CLASS - store and forward
* PROTOCOL_ID - means something in GSM
* PRIORITY_FLAG - interactive
* SCHEDULE_DELIVERY_TIME  - immediate
* VALIDITY_PERIOD - default
* REGISTERED_DELIVERY - receipt please
* REPLACE_IF_PRESENT_FLAG - don't replace
* DATA_CODING - Latin-1 - ISO-8859-1
* SM_DEFAULT_MSG_ID - no, use a custom on

Response:

* "00000011" - length
* "80000004" - SUBMIT_SM_RESP
* "00000000" - status OK
* "00000003" - seqeunce number
* "00" - message id=""


## Back to Tab 2 - smsc

Ctrl-C

```
make server-echo
```

## Back to Tab 3 - netcat

```
nc 0.0.0.0 2776 < tmpfifo | xxd
```

## Back to Tab 4 - interactive

### DELIVER_SM

```
echo -n -e "${LEN61}${SUBMIT_SM}${NULL}${SEQ3}${SERVICE_TYPE}${SOURCE_ADDR_TON}${SOURCE_ADDR_NPI}${SOURCE_ADDR}${DEST_ADDR_TON}${DEST_ADDR_NPI}${DESTINATION_ADDR}${ESM_CLASS}${PROTOCOL_ID}${PRIORITY_FLAG}${SCHEDULE_DELIVERY_TIME}${VALIDITY_PERIOD}${REGISTERED_DELIVERY}${REPLACE_IF_PRESENT_FLAG}${DATA_CODING}${SM_DEFAULT_MSG_ID}${SM_LENGTH}${SHORT_MESSAGE}" >&3
```

Response:

* "0000003d" - length 59
* "00000005" - DELIVER_SM
* "00000000" - status.
* "00000001" - sequence number (brand new, not message ID)
* "0000003400" - SMPP 3.4 (null-terminated)
* "0000" - TON+NPI of source address
* "34343731313132323232323200" - source="447111222222" (dest of MT)
* "0000" - TON and NPI of the destination address
* "34343730303031323331323300" - dest="447000123123"
* Continues with echo of original message.

In reality message body will contain info e.g. message id, success code.

Success code of 4 means "delivered successfully".

Message ID may be in a TLV instead.

### Finally - DELIVER_SM_RESP, and UNBIND

Left as an exercise for the reader.

Use Wireshark to understand SMPP
