# How to send a text message

Slides for a video about how to send an SMS message using SMPP in
a terminal using Netcat and Bash.

Related blog post: https://www.artificialworlds.net/blog/2020/08/10/how-to-send-an-sms-using-netcat-via-smpp/
